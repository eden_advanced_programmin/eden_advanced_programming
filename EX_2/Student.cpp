#include <iostream>
#include "Student.h"

void Student::init(std::string name, Course** courses, unsigned int crsCount)
{
	this->_name = name;
	this->_courses = courses;
	this->_crsCount = crsCount;
}

std::string Student::getName()
{
	return this->_name;
}

void Student::setName(std::string name)
{
	this->_name = name;
}

unsigned int Student::getCrsCount()
{
	return this->_crsCount;
}

Course** Student::getCourses()
{
	return this->_courses;
}

double Student::getAvg()
{
	double avg;
	int i;
	for (i = 0; i < _crsCount; i++)
	{
		avg += this->_courses[i]->getFinalGrade();
	}
	avg /= this->_crsCount;

	return avg;
}